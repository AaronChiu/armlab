import lcm
import time
import numpy as np
import math


from lcmtypes import dynamixel_command_t
from lcmtypes import dynamixel_command_list_t
from lcmtypes import dynamixel_status_t
from lcmtypes import dynamixel_status_list_t

PI = np.pi
D2R = PI/180.0
R2D = 180.0/PI
ANGLE_TOL = 2*PI/180.0 


""" Rexarm Class """
class Rexarm():
    def __init__(self):

        """ Commanded Values """
        self.joint_angles = [0.0, 0.0, 0.0, 0.0] # radians
        # you SHOULD change this to contorl each joint speed separately 
        self.joint_speed = [0.5,0.5,0.5,0.5]
        self.speed = 0.5                         # 0 to 1
        self.max_torque = 0.5                    # 0 to 1

        """ Feedback Values """
        self.joint_angles_fb = [0.0, 0.0, 0.0, 0.0] # radians
        self.speed_fb = [0.0, 0.0, 0.0, 0.0]        # 0 to 1   
        self.load_fb = [0.0, 0.0, 0.0, 0.0]         # -1 to 1  
        self.temp_fb = [0.0, 0.0, 0.0, 0.0]         # Celsius               

        """ Plan - TO BE USED LATER """
        self.plan = []
        self.speed_plan = []
        self.plan_status = 0
        self.wpt_number = 0
        self.wpt_total = 0
        self.timetable = []
        self.feedback = []
        self.speedfeedback = []
        self.reachtime = []
	self.worldCoord = ()
	self.troZmatrix = np.float32([])
	self.transZmatrix = np.float32([])
	self.transXmatrix = np.float32([])
	self.troXmatrix = np.float32([])
	
	self.T = np.float32([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]])
	self.TM = np.float32([])
        
        self.linklength = np.float32([114.8,101.1,101.1,106.1])

        """ LCM Stuff"""
        self.lc = lcm.LCM()
        lcmMotorSub = self.lc.subscribe("ARM_STATUS",
                                        self.feedback_handler)

    def cmd_publish(self):
        """ 
        Publish the commands to the arm using LCM. 
        NO NEED TO CHANGE.
        You need to activelly call this function to command the arm.
        You can uncomment the print statement to check commanded values.
        """    
        msg = dynamixel_command_list_t()
        msg.len = 4
        self.clamp()
        for i in range(msg.len):
            cmd = dynamixel_command_t()
            cmd.utime = int(time.time() * 1e6)
            cmd.position_radians = self.joint_angles[i]
            # you SHOULD change this to contorl each joint speed separately 
            cmd.speed = self.joint_speed[i]
            #print cmd.speed
            cmd.max_torque = self.max_torque
            #print cmd.position_radians
            msg.commands.append(cmd)
            #print cmd.speed
        self.lc.publish("ARM_COMMAND",msg.encode())
    
    def get_feedback(self):
        """
        LCM Handler function
        Called continuously from the GUI 
        NO NEED TO CHANGE
        """
        self.lc.handle_timeout(50)

    def feedback_handler(self, channel, data):
        """
        Feedback Handler for LCM
        NO NEED TO CHANGE FOR NOW.
        LATER NEED TO CHANGE TO MANAGE PLAYBACK FUNCTION 
        """
        msg = dynamixel_status_list_t.decode(data)
        for i in range(msg.len):
            self.joint_angles_fb[i] = msg.statuses[i].position_radians 
            self.speed_fb[i] = msg.statuses[i].speed 
            self.load_fb[i] = msg.statuses[i].load 
            self.temp_fb[i] = msg.statuses[i].temperature

    def clamp(self):
        """
        Clamp Function
        Limit the commanded joint angles to ones physically possible so the 
        arm is not damaged.
        TO DO: IMPLEMENT SUCH FUNCTION
        """
	if self.joint_angles[1] > 122*D2R:
            self.joint_angles[1] = 122*D2R
	elif  self.joint_angles[1] < -126*D2R:
            self.joint_angles[1] = -126*D2R
	if self.joint_angles[2] > 124*D2R:
            self.joint_angles[2] = 124*D2R
	elif  self.joint_angles[2] < -120*D2R:
            self.joint_angles[2] = -120*D2R
	if self.joint_angles[3] > 126*D2R:
            self.joint_angles[3] = 126*D2R
	elif  self.joint_angles[3] < -128*D2R:
            self.joint_angles [3] = -128*D2R
	
 
        

    def plan_command(self):
        """ Command waypoints - TO BE ADDED LATER """
        if self.plan_status == 0:
            pass
        else:
            print "1 " + str(self.plan)
            self.joint_angles = self.plan[self.wpt_number]
            #self.joint_speed = self.speed_plan[self.wpt_number]
            
            self.speedfeedback.append(self.speed_fb[:])
            self.feedback.append(self.joint_angles_fb[:])
            #print "joint" + str(self.joint_angles_fb)
            self.timetable.append(time.time())
            if np.linalg.norm(np.float32(self.joint_angles)-np.float32(self.joint_angles_fb)) < 0.04:# and self.wpt_number<self.wpt_total-1:
                self.reachtime.append(time.time())
                #print self.joint_angles
                #print self.plan
                if self.wpt_number == self.wpt_total -1:
                    self.plan_status =0
                    self.reachtime.append(time.time())
                    #print "feedback" + str(self.feedback)
                    #print "time" + str(self.timetable)
                    np.savetxt("feedback.csv", self.feedback, '%.18f', delimiter=",")
                    np.savetxt("timetable.csv", self.timetable, '%.6f', delimiter=",")
                    np.savetxt("reachtime.csv", self.reachtime, '%.6f', delimiter=",")
                    np.savetxt("speedfeedbaack.csv", self.speedfeedback,'%.18f',delimiter=",")
                self.wpt_number += 1
            
            #print self.plan_status
            self.joint_speed = [0.9,0.9,0.9,0.9]
            #print self.joint_speed
            #self.torque = 0.3
            #print self.speed_fb
            self.torque = 1.0
            self.cmd_publish()
            

    def troZ(self,theta):
	self.troZmatrix = np.float32([[np.cos(theta),-np.sin(theta),0,0],[np.sin(theta),np.cos(theta),0,0],[0,0,1,0],[0,0,0,1]])
	return self.troZmatrix
	
    def transZ(self,d):
	self.transZmatrix = np.float32([[1,0,0,0],[0,1,0,0],[0,0,1,d],[0,0,0,1]])
	return self.transZmatrix

    def transX(self,a):
	self.transXmatrix = np.float32([[1,0,0,a],[0,1,0,0],[0,0,1,0],[0,0,0,1]])
	return self.transXmatrix

    def troX(self,theta):
	self.troXmatrix = np.float32([[1,0,0,0],[0,np.cos(theta),-np.sin(theta),0],[0,np.sin(theta),np.cos(theta),0],[0,0,0,1]])
	return self.troXmatrix

    def rexarm_FK(self,dh_table,link):
        
	
	for i in range(link):
	    #print self.troZ(dh_table[i][0])
	    
	    self.TM = self.troZ(dh_table[i][0]).dot(self.transZ(dh_table[i][1])).dot(self.transX(dh_table[i][2])).dot(self.troX(dh_table[i][3]))
	    self.T = self.T.dot(self.TM)
        if self.joint_angles_fb[0] >= -PI/2 and self.joint_angles_fb[0] <= PI/2 :
            if self.T[0][3] >= 0:
                self.phi = dh_table[2][0]+dh_table[3][0]+dh_table[4][0]-PI/2
            else:
                self.phi = -dh_table[2][0] - dh_table[3][0] - dh_table[4][0]-PI/2
        else:
             if self.T[0][3] <= 0:
                self.phi = dh_table[2][0]+dh_table[3][0]+dh_table[4][0]-PI/2
             else:
                self.phi = -dh_table[2][0] - dh_table[3][0] - dh_table[4][0]-PI/2

	self.worldCoord = (self.T[0][3],self.T[1][3],self.T[2][3],self.phi*R2D)
	self.T = np.float32([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]])	
	"""
        Calculates forward kinematics for rexarm
        takes a DH table filled with DH parameters of the arm
        and the link to return the position for
        returns a 4-tuple (x, y, z, phi) representing the pose of the 
        desired link
        """
        
    	
    def rexarm_IK(self,pose, cfg):
        pose_sb = pose[3]*D2R
        theta_1 = np.arctan2(pose[1],pose[0]) 
        z_3_0 = self.linklength[3]*np.sin(pose_sb) + pose[2]
        x_3_0 = pose[0] - self.linklength[3]*np.cos(theta_1)*np.cos(pose_sb)
        y_3_0 = pose[1] - self.linklength[3]*np.cos(pose_sb)*np.sin(theta_1)
        #print [x_3_0,y_3_0,z_3_0]
        if np.power(x_3_0,2) + np.power(y_3_0,2) + np.power(z_3_0-self.linklength[0],2) >= np.power(self.linklength[1]+self.linklength[2],2):
            print "The specified tooltip workspace configuration is not reachable.1"
            return None
        else:
            D = (np.power(x_3_0,2) + np.power(y_3_0,2) + np.power(z_3_0-self.linklength[0],2) - np.power(self.linklength[1],2) - np.power(self.linklength[2],2))/(2*self.linklength[1]*self.linklength[2])
            #print [x_3_0, y_3_0, z_3_0,D]
            #print pose
            if cfg == 1:
                theta_3  = np.arctan2(-np.sqrt(1-np.power(D,2)), D)

            theta_2 = np.arctan2(z_3_0-self.linklength[0],np.sqrt(np.power(x_3_0,2) + np.power(y_3_0,2))) - np.arctan2(self.linklength[2]*np.sin(theta_3),self.linklength[1] + self.linklength[2]*np.cos(theta_3)) - PI/2
        
            #theta_4 = (-theta_2 - theta_3 - pose[4]) - PI/2
        
            if theta_1 >= -PI/2 and theta_1 <= PI/2 :
                if pose[0] >= 0:
                    theta_4 = -theta_2 - theta_3 - pose_sb - PI/2
                else:
                    theta_4 = -theta_2 - theta_3 + pose_sb + PI/2
            else:
                if pose[0] <= 0:
                    theta_4 = -theta_2 - theta_3 - pose_sb - PI/2
                else:
                    theta_4 = -theta_2 - theta_3 + pose_sb + PI/2
            #print [theta_1*R2D,theta_2*R2D,theta_3*R2D,theta_4*R2D]
            if theta_2 > 122*D2R or theta_2 < -126*D2R or theta_3 > 124*D2R or theta_3 < -120*D2R or theta_4 > 126*D2R or theta_4 < -128*D2R:
                print "The specified tooltip workspace configuration is not reachable.2"
                return None
            else:
                return [theta_1,theta_2,theta_3,theta_4]
        """
        Calculates inverse kinematics for the rexarm
        pose is a tuple (x, y, z, phi) which describes the desired
        end effector position and orientation.  
        cfg describe elbow down (0) or elbow up (1) configuration
        returns a 4-tuple of joint angles or NONE if configuration is impossible
        """
        
        
    def rexarm_collision_check(q):
        """
        Perform a collision check with the ground and the base
        takes a 4-tuple of joint angles q
        returns true if no collision occurs
        """
        pass 
	
    
