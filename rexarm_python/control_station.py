import sys
import cv2
import numpy as np
import time
from PyQt4 import QtGui, QtCore, Qt
from ui import Ui_MainWindow
from rexarm import Rexarm

#from matplotlib import pyplot as plt
import matplotlib.pylab as pylab
from video import Video

""" Radians to/from  Degrees conversions """
D2R = 3.141592/180.0
R2D = 180.0/3.141592
PI = np.pi

""" Pyxel Positions of image in GUI """
MIN_X = 310
MAX_X = 950

MIN_Y = 30
MAX_Y = 510
 
class Gui(QtGui.QMainWindow):
    """ 
    Main GUI Class
    It contains the main function and interfaces between 
    the GUI and functions
    """
    def __init__(self,parent=None):
	
	
	
        QtGui.QWidget.__init__(self,parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        """ Main Variables Using Other Classes"""
        self.rex = Rexarm()
        cap = cv2.VideoCapture(0)
        self.video = Video(cap)
        #self.video.w = int(cap.get(3))
        #self.video.h = int(cap.get(4))
        #print self.video.w

        """ Other Variables """
        self.last_click = np.float32([0,0])

        """ Set GUI to track mouse """
        QtGui.QWidget.setMouseTracking(self,True)

        """ 
        Video Function 
        Creates a timer and calls play() function 
        according to the given time delay (27mm) 
        """
        self._timer = QtCore.QTimer(self)
        self._timer.timeout.connect(self.play)
        self._timer.start(27)
       
        """ 
        LCM Arm Feedback
        Creates a timer to call LCM handler continuously
        No delay implemented. Reads all time 
        """  
        self._timer2 = QtCore.QTimer(self)
        self._timer2.timeout.connect(self.rex.get_feedback)
        self._timer2.start()
	
	self._timer3 = QtCore.QTimer(self)
        self._timer3.timeout.connect(self.rex.plan_command)
        self._timer3.start()
        """ 
        Connect Sliders to Function
        TO DO: CONNECT THE OTHER 5 SLIDERS IMPLEMENTED IN THE GUI 
        """ 
        self.ui.sldrBase.valueChanged.connect(self.slider_change)
	self.ui.sldrShoulder.valueChanged.connect(self.slider_change)
        self.ui.sldrElbow.valueChanged.connect(self.slider_change)
	self.ui.sldrWrist.valueChanged.connect(self.slider_change)
	        
	self.ui.sldrMaxTorque.valueChanged.connect(self.slider_change)
	self.ui.sldrSpeed.valueChanged.connect(self.slider_change)

	
	
	""" Commands the arm as the arm initialize to 0,0,0,0 angles """
        self.slider_change() 
        
        """ Connect Buttons to Functions """
        self.ui.btnLoadCameraCal.clicked.connect(self.load_camera_cal)
        self.ui.btnPerfAffineCal.clicked.connect(self.affine_cal)
        self.ui.btnTeachRepeat.clicked.connect(self.tr_initialize)
        self.ui.btnAddWaypoint.clicked.connect(self.tr_add_waypoint)
        self.ui.btnSmoothPath.clicked.connect(self.tr_smooth_path)
        self.ui.btnPlayback.clicked.connect(self.tr_playback)
        self.ui.btnDefineTemplate.clicked.connect(self.def_template)
        self.ui.btnLocateTargets.clicked.connect(self.template_match)
        self.ui.btnExecutePath.clicked.connect(self.exec_path)

	#self.camera_matrix = np.genfromtxt('test.csv',delimiter=",")
	
	self.worldcal = np.float32([[305],[-305],[305],[305],[-305],[305]])
	
	self.waypoint = []
	self.smoothwpt = []
	self.smoothspeed = []
	self.dhtable = np.float32([])
	self.link = 5
	self.linklength = np.float32([114.8,101.1,101.1,106.1])
	self.midspeed = 0.2  
        self.finalspeed = 0.001 
	self.smoothtime = 0.25
        self.step = 0.05
	self.qmatrix = []

	self.elbowcfg = 1 #up

        self.grayFrame = []
        self.temp_array = []

        self.pt_list = []
	self.vertex_list = []
        self.vertex_world_temp = []
        self.world_temp_neg = []
        self.world_temp_pos = []
        self.vertex_world = []
        self.vertex_world_list = []
        self.exe_wp_world = []
        self.exe_wp = []
        self.exe = []
        self.angle_list = []

        self.phi_min = 20
        self.phi_max = 160
        self.phi_flag = [0,0,0]

        self.exe_h2 = 60.0
        self.exe_h1 = 30.0

        self.delete_wp = []

        self.target_phi = []
        self.h1_phi = []
        self.h2_phi = []

        self.FKWorldCoord = []
        self.FKtimetable = []

    def play(self):
        """ 
        Play Funtion
        Continuously called by GUI 
        """

        """ Renders the Video Frame """
        try:
            self.video.captureNextFrame()
            self.ui.videoFrame.setPixmap(self.video.convertFrame())
            self.ui.videoFrame.setScaledContents(True)
        except TypeError:
            print "No frame"
        
        
        

        """ 
        Update GUI Joint Coordinates Labels
        TO DO: include the other slider labels 
        """
        
        self.ui.rdoutBaseJC.setText(str(self.rex.joint_angles_fb[0]*R2D))
        self.ui.rdoutShoulderJC.setText(str(self.rex.joint_angles_fb[1]*R2D))
        self.ui.rdoutElbowJC.setText(str(self.rex.joint_angles_fb[2]*R2D))
        self.ui.rdoutWristJC.setText(str(self.rex.joint_angles_fb[3]*R2D))
		
		
	self.dhtable = np.float32([[self.rex.joint_angles_fb[0],self.linklength[0],0,-PI/2],[-PI/2,0,0,0],[-self.rex.joint_angles_fb[1],0,self.linklength[1],0],[-self.rex.joint_angles_fb[2],0,self.linklength[2],0],[-self.rex.joint_angles_fb[3],0,self.linklength[3],0]])
		
	#print self.dhtable
	
	self.rex.rexarm_FK(self.dhtable,self.link)
        self.FKtimetable.append(time.time())
        self.FKWorldCoord.append(self.rex.worldCoord[:])
       
        np.savetxt("FKtimetable.csv", self.FKtimetable, '%.6f', delimiter=",")
        np.savetxt("FkWorldCoord.csv", self.FKWorldCoord, '%5.3f', delimiter=",")
        
	#self.rex.rexarm_FK([],4)

	self.ui.rdoutX.setText(str(self.rex.worldCoord[0]))
	self.ui.rdoutY.setText(str(self.rex.worldCoord[1]))
	self.ui.rdoutZ.setText(str(self.rex.worldCoord[2]))
	self.ui.rdoutT.setText(str(self.rex.worldCoord[3]))
        """ 
        Mouse position presentation in GUI
        TO DO: after getting affine calibration make the apprriate label
        to present the value of mouse position in world coordinates 
        """
			
	#camera_matrix = np.genfromtxt('test.csv',delimiter=",")
	#print camera_matrix			
			
				    
        x = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).x()
        #print x
        y = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).y()
        #print y
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)):
            self.ui.rdoutMousePixels.setText("(-,-)")
            self.ui.rdoutMouseWorld.setText("(-,-)")
        else:
            x = x - MIN_X
            y = y - MIN_Y
            self.ui.rdoutMousePixels.setText("(%.0f,%.0f)" % (x,y))
            if (self.video.aff_flag == 2):
		world_coord = np.dot(self.video.aff_matrix,np.float32([[x],[y],[1.0]])) 
		                
		""" TO DO Here is where affine calibration must be used """
                #self.ui.rdoutMouseWorld.setText("(-,-)")
		self.ui.rdoutMouseWorld.setText("(%.0f,%.0f)" % (-world_coord[1][0],-world_coord[0][0]))
            else:
                self.ui.rdoutMouseWorld.setText("(-,-)")

        """ 
        Updates status label when rexarm playback is been executed.
        This will be extended to includ eother appropriate messages
        """ 
        if(self.rex.plan_status == 1):
            self.ui.rdoutStatus.setText("Playing Back - Waypoint %d"
                                    %(self.rex.wpt_number + 1))


    def slider_change(self):
        """ 
        Function to change the slider labels when sliders are moved
        and to command the arm to the given position 
        TO DO: Implement for the other sliders
        """
	
	self.ui.rdoutTorq.setText(str(self.ui.sldrMaxTorque.value()) + "%")
       	self.rex.max_torque = self.ui.sldrMaxTorque.value()/100.0
	
	
	self.ui.rdoutSpeed.setText(str(self.ui.sldrSpeed.value()))
	self.rex.speed = self.ui.sldrSpeed.value()
        self.rex.joint_speed = [self.ui.sldrSpeed.value(), self.ui.sldrSpeed.value(), self.ui.sldrSpeed.value(), self.ui.sldrSpeed.value()]
	

	self.ui.rdoutBase.setText(str(self.ui.sldrBase.value()))
        self.rex.joint_angles[0] = self.ui.sldrBase.value()*D2R
		

	self.ui.rdoutShoulder.setText(str(self.ui.sldrShoulder.value()))
        self.rex.joint_angles[1] = self.ui.sldrShoulder.value()*D2R
		

	self.ui.rdoutElbow.setText(str(self.ui.sldrElbow.value()))
        self.rex.joint_angles[2] = self.ui.sldrElbow.value()*D2R
	
	
	self.ui.rdoutWrist.setText(str(self.ui.sldrWrist.value()))
        self.rex.joint_angles[3] = self.ui.sldrWrist.value()*D2R

        self.rex.cmd_publish()

    def mousePressEvent(self, QMouseEvent):
        """ 
        Function used to record mouse click positions for 
        affine calibration 
        """
 
        """ Get mouse posiiton """
        x = QMouseEvent.x()
        y = QMouseEvent.y()

        """ If mouse position is not over the camera image ignore """
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)): return

        """ Change coordinates to image axis """
        self.last_click[0] = x - MIN_X
        self.last_click[1] = y - MIN_Y
       
        """ If affine calibration is been performed """
        if (self.video.aff_flag == 1):
            """ Save last mouse coordinate """
            self.video.mouse_coord[self.video.mouse_click_id] = [(x-MIN_X),
                                                                 (y-MIN_Y)]

            """ Update the number of used poitns for calibration """
            self.video.mouse_click_id += 1

            """ Update status label text """
            self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                      %(self.video.mouse_click_id + 1))

            """ 
            If the number of click is equal to the expected number of points
            computes the affine calibration.
            TO DO: Change this code to use you programmed affine calibration
            and NOT openCV pre-programmed function as it is done now.
            """
            if(self.video.mouse_click_id == self.video.aff_npoints):
                """ 
                Update status of calibration flag and number of mouse
                clicks
                """
                self.video.aff_flag = 2
                self.video.mouse_click_id = 0
                
                """ Perform affine calibration with OpenCV """
                #self.video.aff_matrix = cv2.getAffineTransform(
                #                        self.video.mouse_coord,
                #                        self.video.real_coord)
		A = np.float32([[self.video.mouse_coord[0][0],self.video.mouse_coord[0][1],1,0,0,0],[0,0,0,self.video.mouse_coord[0][0],self.video.mouse_coord[0][1],1],[self.video.mouse_coord[1][0],self.video.mouse_coord[1][1],1,0,0,0],[0,0,0,self.video.mouse_coord[1][0],self.video.mouse_coord[1][1],1],[self.video.mouse_coord[2][0],self.video.mouse_coord[2][1],1,0,0,0],[0,0,0,self.video.mouse_coord[2][0],self.video.mouse_coord[2][1],1]])
		
		
            	aff_vector = np.dot(np.linalg.inv(A),self.worldcal)
		
                """ Updates Status Label to inform calibration is done """ 
                self.ui.rdoutStatus.setText("Waiting for input")

                """ 
                Uncomment to gether affine calibration matrix numbers 
                on terminal
                """ 
               	#print self.video.aff_matrix
		self.video.aff_matrix = np.float32([[aff_vector[0][0],aff_vector[1][0],aff_vector[2][0]],[aff_vector[3][0],aff_vector[4][0],aff_vector[5][0]],[0.0,0.0,1.0]])
                
        """ If def template is been performed """
        if (self.video.def_temp_flag == 1):
            #self.video.mouse_coord_def_temp[self.video.mouse_click_id] = [(x-MIN_X),(y-MIN_Y)] # Save last mouse coordinate
            self.video.mouse_coord_def_temp[self.video.mouse_click_id] = [(x-MIN_X)*2,(y-MIN_Y)*2]
            self.video.mouse_click_id += 1 # Update the number of used poitns for calibration
            if(self.video.mouse_click_id == self.video.def_temp_npoints):
                self.video.def_temp_flag = 2
                self.video.mouse_click_id = 0
                self.temp_array = np.zeros((self.video.mouse_coord_def_temp[1][1]-self.video.mouse_coord_def_temp[0][1]+1, self.video.mouse_coord_def_temp[1][0]-self.video.mouse_coord_def_temp[0][0]+1))
                self.temp_array.tolist()
                #print "temp" + str(self.temp_array.shape)
                #print self.video.mouse_coord_def_temp[0]
                #print self.video.mouse_coord_def_temp[1]
                #print (self.video.mouse_coord_def_temp[1][1]-self.video.mouse_coord_def_temp[0][1]+1, self.video.mouse_coord_def_temp[1][0]-self.video.mouse_coord_def_temp[0][0]+1)
                for index_y in range(int(self.video.mouse_coord_def_temp[0][1]), int(self.video.mouse_coord_def_temp[1][1]+1)):
                    for index_x in range(int(self.video.mouse_coord_def_temp[0][0]),int(self.video.mouse_coord_def_temp[1][0]+1)):
                        self.temp_array[index_y-int(self.video.mouse_coord_def_temp[0][1])][index_x-int(self.video.mouse_coord_def_temp[0][0])] = self.grayFrame[index_y][index_x]
                #print self.video.mouse_coord_def_temp[0]
                #print self.video.mouse_coord_def_temp[1]
                #print self.temp_array
                #print self.temp_array.shape
                #pylab.matshow(self.grayFrame)
                #pylab.show()
                #pylab.matshow(self.temp_array)
                #pylab.show()
		
    def jump_list(self, mid, step, min):
        a = []
        number = (mid - min)/step
        a.append(mid)
        for i in range(1,number):
            a.append(mid - i*step)
            a.append(mid + i*step)
        return a

    def affine_cal(self):
        """ 
        Function called when affine calibration button is called.
        Note it only chnage the flag to record the next mouse clicks
        and updates the status text label 
        """
        self.video.aff_flag = 1 
        self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                    %(self.video.mouse_click_id + 1))

    def load_camera_cal(self):
        print "Load Camera Cal"

    def tr_initialize(self):
	print "Teach and Repeat"
	self.rex.max_torque = 0
	self.rex.cmd_publish()
	self.waypoint = []
	
    def tr_add_waypoint(self):
        print "Add Waypoint"
        #self.waypoint.append(self.rex.joint_angles_fb[:])

        """circle"""
        for t in range(20):
            x = (200 - 10*t)*np.cos(2*PI/20*t+PI+0.001)
            y = (200 - 10*t)*np.sin(2*PI/20*t+PI+0.001)
            z = 200+10*t
            phi = -PI/2*R2D
            self.waypoint.append(self.rex.rexarm_IK((x,y,z,phi),self.elbowcfg)[:])
        
        """8"""
        #for t in range(20):
            #y = 40*np.cos(2*PI/20*t+PI+0.001)
            #z = 40*np.sin(2*PI/20*t+PI+0.001)+80
            #x = 250
            #phi = 0
            #self.waypoint.append(self.rex.rexarm_IK((x,y,z,phi),self.elbowcfg)[:])

        #print "inverse kinematics"
			    
        #self.waypoint.append(self.rex.rexarm_IK(self.rex.worldCoord,self.elbowcfg)[:])
	#self.waypoint.append(self.rex.rexarm_IK((200,200,200,200),self.elbowcfg)[:])
        #self.waypoint.append(self.rex.rexarm_IK((80,80,80,0),self.elbowcfg)[:])
	#print self.waypoint
	#print "waypoint" + str(self.waypoint)

    def tr_smooth_path(self):
        """
       	print "3.2 Smooth Path"
	self.smoothwpt.append([self.waypoint[0][0], self.waypoint[0][1]+40*D2R, self.waypoint[0][2], self.waypoint[0][3]])
	self.smoothwpt.append(self.waypoint[0][:])
	for index in range(1, len(self.waypoint)):
		self.smoothwpt.append([self.waypoint[index-1][0], self.waypoint[index-1][1]+40*D2R, self.waypoint[index-1][2], self.waypoint[index-1][3]])

		self.smoothwpt.append([self.waypoint[index][0], self.waypoint[index-1][1]+40*D2R, self.waypoint[index-1][2], self.waypoint[index-1][3]])
		self.smoothwpt.append([self.waypoint[index][0], self.waypoint[index-1][1]+40*D2R, self.waypoint[index][2], self.waypoint[index-1][3]])

		self.smoothwpt.append(self.waypoint[index][:])
	"""	
        print "3.3 smooth path"
        for k in range(len(self.waypoint)-1):
            self.qmatrix = []
            for i in range(4):
                a0 = self.waypoint[k][i]
                if k%2 == 0:
                    a1 = self.midspeed
                else:
                    a1 = self.midspeed
                    
                a2 = (3*(self.waypoint[k+1][i]-self.waypoint[k][i])-(2*self.midspeed+self.finalspeed)*self.smoothtime)/np.power(self.smoothtime,2)
                a3 = (2*(self.waypoint[k][i]-self.waypoint[k+1][i])+(self.midspeed+self.finalspeed)*self.smoothtime)/np.power(self.smoothtime,3)
                self.qmatrix.append([a0,a1,a2,a3])
                #print "qmatrix" + str(self.qmatrix)

            
            for j in range(0,int(self.smoothtime/self.step)):
                qbase = self.qmatrix[0][0] + self.qmatrix[0][1]*j*self.step+self.qmatrix[0][2]*np.power(j*self.step,2) + self.qmatrix[0][3]*np.power(j*self.step,3)
                qshoulder = self.qmatrix[1][0] + self.qmatrix[1][1]*j*self.step+self.qmatrix[1][2]*np.power(j*self.step,2) + self.qmatrix[1][3]*np.power(j*self.step,3)
                qelbow = self.qmatrix[2][0] + self.qmatrix[2][1]*j*self.step+self.qmatrix[2][2]*np.power(j*self.step,2) + self.qmatrix[2][3]*np.power(j*self.step,3)
                qwrist = self.qmatrix[3][0] + self.qmatrix[3][1]*j*self.step+self.qmatrix[3][2]*np.power(j*self.step,2) + self.qmatrix[3][3]*np.power(j*self.step,3)
                self.smoothwpt.append([qbase,qshoulder,qelbow,qwrist])
           
            
                vbase = self.qmatrix[0][1] + 2*self.qmatrix[0][2]*j*self.step + 3*self.qmatrix[0][3]*np.power(j*self.step,2)
                vshoulder = self.qmatrix[1][1] + 2*self.qmatrix[1][2]*j*self.step + 3*self.qmatrix[1][3]*np.power(j*self.step,2)
                velbow = self.qmatrix[2][1] + 2*self.qmatrix[2][2]*j*self.step + 3*self.qmatrix[2][3]*np.power(j*self.step,2)
                vwrist = self.qmatrix[3][1] + 2*self.qmatrix[3][2]*j*self.step + 3*self.qmatrix[3][3]*np.power(j*self.step,2)
                self.smoothspeed.append([abs(vbase),abs(vshoulder),abs(velbow),abs(vwrist)])
                #self.smoothspeed.append([vbase,vshoulder,velbow,vwrist])
                
        self.smoothwpt.append(self.waypoint[len(self.waypoint)-1])
        self.smoothspeed.append([self.finalspeed,self.finalspeed,self.finalspeed,self.finalspeed])
       # print self.smoothwpt
       # print "smooth" + str(self.smoothspeed)
        #print "waypoint" + str(self.waypoint)
        
    def tr_playback(self):
        print "Playback"

        """normal"""
        np.savetxt("waypoints.csv", self.waypoint,'%5.3f',delimiter=",")
        np.savetxt("smoothwaypoints.csv", self.smoothwpt,'%5.3f',delimiter=",")
        np.savetxt("smoothspeed.csv", self.smoothspeed,'%5.3f',delimiter=",")

	#self.rex.plan = self.waypoint 
	#self.rex.plan = self.smoothwpt
	#self.rex.speed_plan = self.smoothspeed
        self.rex.plan = self.exe
	self.rex.plan_status = 1
	self.rex.wpt_number = 0
	self.rex.wpt_total = len(self.rex.plan)
	
	print self.rex.plan
	

    def def_template(self):
        print "Define Template"
        self.video.def_temp_flag = 1
        self.grayFrame = cv2.cvtColor(self.video.currentFrame, cv2.COLOR_BGR2GRAY)
        cv2.imwrite('big.png', self.grayFrame)
        #print self.video.currentFrame.shape
        #print self.grayFrame.shape
        #print self.grayFrame[300][400]

    def our_match_temp(self, img_s, img_t):
        img_s = np.float32(img_s)
        img_t = np.float32(img_t)
        s_row = img_s.shape[0]
        s_col = img_s.shape[1]
        t_row = img_t.shape[0]
        t_col = img_t.shape[1]
        res = np.zeros((s_row, s_col))
        for index_i in range(s_row - t_row+1):
            for index_j in range(s_col - t_col + 1):
                dem = 0
                num1 = 0
                num2 = 0
                for index_k in range(t_row):
                    for index_l in range(t_col):
                        p_img_s = img_s[index_i+index_k][index_j+index_l]
                        p_img_t = img_t[index_k][index_l]
                        dem = dem + p_img_t*p_img_s
                        num1 = num1 + np.power(p_img_t,2)
                        num2 = num2 + np.power(p_img_s,2)
                res[index_i][index_j] = dem/np.power(num1*num2,0.5)
            print index_i
        return res
        
    def template_match(self):
        print "Template Match"
        #print self.grayFrame
        #print self.temp_array
        cv2.imwrite('temp1.png',self.temp_array)
        img_big = cv2.imread('big.png',0)
        img_temp = cv2.imread('temp.png',0)
        #print img_big
        #res = self.our_match_temp(img_big, img_temp)
        res = cv2.matchTemplate(img_big,img_temp,cv2.TM_CCOEFF_NORMED)
        #print res
        #print np.amax(res)
        #print res.shape
        pylab.matshow(res)
        pylab.show()
        w, h = img_temp.shape[::-1]
        threshold = 0.85
        loc = np.where( res >= threshold)
        #print loc
        for pt in zip(*loc[::-1]):
            #print pt
            cv2.rectangle(img_big, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)
            self.pt_list.append([pt[0],pt[1]])
        count = 0
        final_count = count
        for index in range(len(self.pt_list)-1):
            if (self.pt_list[index][0] == self.pt_list[index+1][0]-1 or self.pt_list[index][1] == self.pt_list[index+1][1]-1):
                count = count +1
            else:
                if count%2 == 0:
                    self.vertex_list.append(self.pt_list[index - count/2])
                else:
                    self.vertex_list.append(self.pt_list[index - count/2 - 1])
                count = 0
                final_count = count
            #print count
        if final_count%2 == 0:
            self.vertex_list.append(self.pt_list[index  - final_count/2])
        else:
            self.vertex_list.append(self.pt_list[index  - final_count/2-1])
        for index in range(len(self.vertex_list)):
            self.vertex_list[index] = [int((self.vertex_list[index][0]+w/2)/2), int((self.vertex_list[index][1]+h/2)/2)]
            self.vertex_world_temp.append(np.dot(self.video.aff_matrix,np.float32([[self.vertex_list[index][0]], [self.vertex_list[index][1]], [1.0]])))
            if np.arctan2(-self.vertex_world_temp[index][0][0], -self.vertex_world_temp[index][1][0]) < 0:
                self.world_temp_neg.append([-self.vertex_world_temp[index][1][0], -self.vertex_world_temp[index][0][0], 15.0, np.arctan2(-self.vertex_world_temp[index][0][0], -self.vertex_world_temp[index][1][0])])
            else:
                self.world_temp_pos.append([-self.vertex_world_temp[index][1][0], -self.vertex_world_temp[index][0][0], 15.0, np.arctan2(-self.vertex_world_temp[index][0][0], -self.vertex_world_temp[index][1][0])])
        self.world_temp_neg = np.array(self.world_temp_neg)
        self.world_temp_pos = np.array(self.world_temp_pos)
        self.world_temp_neg.view('i8,i8,i8,i8').sort(order=['f3'], axis = 0)
        self.world_temp_pos.view('i8,i8,i8,i8').sort(order=['f3'], axis = 0)
        for index in range(len(self.world_temp_neg)):
            self.vertex_world.append(self.world_temp_neg[len(self.world_temp_neg)-index-1])
        for index in range(len(self.world_temp_pos)):
            self.vertex_world.append(self.world_temp_pos[index])
        self.vertex_world = np.array(self.vertex_world)
        '''change phi'''
        for index in range(len(self.vertex_world)):
            for phi in self.jump_list(90, 1, 20):
                if (self.rex.rexarm_IK((self.vertex_world[index][0], self.vertex_world[index][1], self.vertex_world[index][2], phi), self.elbowcfg) != None):
                    self.phi_flag[0] = 1
            for phi in self.jump_list(90, 1, 20):
                if (self.rex.rexarm_IK((self.vertex_world[index][0], self.vertex_world[index][1], self.exe_h1, phi), self.elbowcfg) != None):
                    self.phi_flag[1] = 1
            for phi in self.jump_list(90, 1, 20):
                if (self.rex.rexarm_IK((self.vertex_world[index][0], self.vertex_world[index][1], self.exe_h2, phi), self.elbowcfg) != None):
                    self.phi_flag[2] = 1
            if self.phi_flag != [1,1,1]:
                self.delete_wp.append(index)
        for index in range(len(self.vertex_world)):
            if index not in self.delete_wp:
                self.vertex_world_list.append(self.vertex_world[index])

            #self.vertex_world_list.append(np.dot(self.video.aff_matrix,np.float32([[self.vertex_list[index][0]], [self.vertex_list[index][1]], [1.0]])))
            #self.vertex_world_list[index] = [-self.vertex_world_list[index][1][0], -self.vertex_world_list[index][0][0], 10.0, np.arctan2(-self.vertex_world_list[index][0][0], -self.vertex_world_list[index][1][0])]
        #print self.vertex_world_list
        #self.vertex_world_list = np.array(self.vertex_world_list)
        #self.vertex_world_list.view('i8,i8,i8,i8').sort(order=['f3'], axis = 0)
        
        print self.world_temp_neg
        print self.world_temp_pos
        #print self.vertex_list
        print self.vertex_world_list
        #print self.angle_list
        #print self.vertex_world_list
        cv2.imwrite('res.png',img_big)

    def exec_path(self):
        print "Execute Path"
        n_insert_pt = 4.0
        exe_phi = 70.0

        '''straight line
        for index in range(len(self.vertex_world_list)-1):
            self.exe_wp_world.append((self.vertex_world_list[index][0], self.vertex_world_list[index][1], exe_h4, exe_phi))
            self.exe_wp_world.append((self.vertex_world_list[index][0], self.vertex_world_list[index][1], exe_h3, exe_phi))
            self.exe_wp_world.append((self.vertex_world_list[index][0], self.vertex_world_list[index][1], exe_h2, exe_phi))
            self.exe_wp_world.append((self.vertex_world_list[index][0], self.vertex_world_list[index][1], exe_h1, exe_phi))
            self.exe_wp_world.append((self.vertex_world_list[index][0], self.vertex_world_list[index][1], self.vertex_world_list[index][2], exe_phi))
            self.exe_wp_world.append((self.vertex_world_list[index][0], self.vertex_world_list[index][1], exe_h1, exe_phi))
            self.exe_wp_world.append((self.vertex_world_list[index][0], self.vertex_world_list[index][1], exe_h2, exe_phi))
            self.exe_wp_world.append((self.vertex_world_list[index][0], self.vertex_world_list[index][1], exe_h3, exe_phi))
            self.exe_wp_world.append((self.vertex_world_list[index][0], self.vertex_world_list[index][1], exe_h4, exe_phi))
            dx = (self.vertex_world_list[index+1][0] - self.vertex_world_list[index][0])/(n_insert_pt+1.0)
            dy = (self.vertex_world_list[index+1][1] - self.vertex_world_list[index][1])/(n_insert_pt+1.0)
            final_index = index
            for index_j in range(int(n_insert_pt)):
                self.exe_wp_world.append((self.vertex_world_list[index][0]+(dx+1)*index_j, self.vertex_world_list[index][1]+(dy+1)*index_j, exe_hh, exe_phi))
        self.exe_wp_world.append((self.vertex_world_list[final_index+1][0], self.vertex_world_list[final_index+1][1], exe_h4, exe_phi))
        self.exe_wp_world.append((self.vertex_world_list[final_index+1][0], self.vertex_world_list[final_index+1][1], exe_h3, exe_phi))
        self.exe_wp_world.append((self.vertex_world_list[final_index+1][0], self.vertex_world_list[final_index+1][1], exe_h2, exe_phi))
        self.exe_wp_world.append((self.vertex_world_list[final_index+1][0], self.vertex_world_list[final_index+1][1], exe_h1, exe_phi))
        self.exe_wp_world.append((self.vertex_world_list[final_index+1][0], self.vertex_world_list[final_index+1][1], self.vertex_world_list[final_index+1][2], exe_phi))
        self.exe_wp_world.append((self.vertex_world_list[final_index+1][0], self.vertex_world_list[final_index+1][1], exe_h1, exe_phi))
        self.exe_wp_world.append((self.vertex_world_list[final_index+1][0], self.vertex_world_list[final_index+1][1], exe_h2, exe_phi))
        self.exe_wp_world.append((self.vertex_world_list[final_index+1][0], self.vertex_world_list[final_index+1][1], exe_h3, exe_phi))
        self.exe_wp_world.append((self.vertex_world_list[final_index+1][0], self.vertex_world_list[final_index+1][1], exe_h4, exe_phi))
        '''

        ''' no insert points'''
        for index in range(len(self.vertex_world_list)):
            for phi0 in self.jump_list(90, 1, 20):
                if (self.rex.rexarm_IK((self.vertex_world_list[index][0], self.vertex_world_list[index][1], self.vertex_world_list[index][2], phi0), self.elbowcfg) != None):
                    self.target_phi.append(phi0)
                    break
            for phi1 in self.jump_list(90, 1, 20):
                if (self.rex.rexarm_IK((self.vertex_world_list[index][0], self.vertex_world_list[index][1], self.exe_h1, phi1), self.elbowcfg) != None):
                    self.h1_phi.append(phi1)
                    break
            for phi2 in self.jump_list(90, 1, 20):
                if (self.rex.rexarm_IK((self.vertex_world_list[index][0], self.vertex_world_list[index][1], self.exe_h2, phi2), self.elbowcfg) != None):
                    self.h2_phi.append(phi2)
                    break

        for index in range(len(self.vertex_world_list)):
            self.exe_wp_world.append((self.vertex_world_list[index][0], self.vertex_world_list[index][1], self.exe_h2, self.h2_phi[index]))
            self.exe_wp_world.append((self.vertex_world_list[index][0], self.vertex_world_list[index][1], self.exe_h1, self.h1_phi[index]))
            self.exe_wp_world.append((self.vertex_world_list[index][0], self.vertex_world_list[index][1], self.vertex_world_list[index][2], self.target_phi[index]))
            self.exe_wp_world.append((self.vertex_world_list[index][0], self.vertex_world_list[index][1], self.exe_h1, self.h1_phi[index]))
            self.exe_wp_world.append((self.vertex_world_list[index][0], self.vertex_world_list[index][1], self.exe_h2, self.h2_phi[index]))

        print self.exe_wp_world
        for index in range(len(self.exe_wp_world)):
            self.exe_wp.append(self.rex.rexarm_IK(self.exe_wp_world[index], self.elbowcfg)[:])
        for index in range(len(self.exe_wp)):
            self.exe.append(self.exe_wp[index])
            if index%5 == 4 and index <= len(self.exe_wp)-2:
                #self.exe.append([self.exe_wp[index][0],self.exe_wp[index][1],self.exe_wp[index][2]+20*D2R,self.exe_wp[index][3]])
                self.exe.append([self.exe_wp[index][0],self.exe_wp[index][1]+20*D2R,self.exe_wp[index][2],self.exe_wp[index][3]])
                self.exe.append([self.exe_wp[index+1][0],self.exe_wp[index+1][1]+20*D2R,self.exe_wp[index+1][2],self.exe_wp[index+1][3]])
                #self.exe.append([self.exe_wp[index+1][0],self.exe_wp[index+1][1],self.exe_wp[index+1][2]+20*D2R,self.exe_wp[index+1][3]])
 
def main():
    app = QtGui.QApplication(sys.argv)
    ex = Gui()
    ex.show()
    sys.exit(app.exec_())
 
if __name__ == '__main__':
    main()
