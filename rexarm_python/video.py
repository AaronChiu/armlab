import cv2
import numpy as np
from PyQt4 import QtGui, QtCore, Qt

class Video():
    def __init__(self,capture):
        self.capture = capture
        self.capture.set(3, 1280)
        self.capture.set(4, 960)
        self.currentFrame=np.array([])

        """ 
        Affine Calibration Variables 
        Currently only takes three points: center of arm and two adjacent 
        corners of the base board
        Note that OpenCV requires float32 arrays
        """
        self.aff_npoints = 3                                     # Change!
        self.real_coord = np.float32([[0., 0.], [305.,-305.], [-305.,-305.]])
        self.mouse_coord = np.float32([[0.0, 0.0],[0.0, 0.0],[0.0, 0.0]])      
        self.mouse_click_id = 0
        self.aff_flag = 0
        self.aff_matrix = np.float32((2,3))
        self.def_temp_flag = 0
        self.def_temp_npoints = 2
        self.mouse_coord_def_temp = np.float32([[0.0, 0.0],[0.0, 0.0],[0.0, 0.0]])
        self.w = int(capture.get(3))
        self.h = int(capture.get(4))
    
    def captureNextFrame(self):
        """                      
        Capture frame, convert to RGB, and return opencv image      
        """
        ret, frame=self.capture.read()
        #print ret
        current_Frame=cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2BGR)
        #print current_Frame
        camera_matrix = np.float32([[1.90844362e+03,0.00000000e+00,6.82932917e+02],[0.00000000e+00,1.91556973e+03,4.71043101e+02],[0.00000000e+00,0.00000000e+00,1.00000000e+00]])
        dist_coefs = np.float32([-0.24166887,0.06814264,0.00089074,-0.00359129,0.26001626])
        new_camera_matrix, roi = cv2.getOptimalNewCameraMatrix(camera_matrix,dist_coefs,(self.w,self.h),1,(self.w,self.h))
        #print new_camera_matrix
        undistorted = cv2.undistort(current_Frame, camera_matrix, dist_coefs, None, new_camera_matrix)
        if(ret==True):
            #self.currentFrame=cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2BGR)
            #print undistorted
            self.currentFrame = undistorted

    def convertFrame(self):
        """ Converts frame to format suitable for QtGui  """
        try:
            height,width=self.currentFrame.shape[:2]
            img=QtGui.QImage(self.currentFrame,
                              width,
                              height,
                              QtGui.QImage.Format_RGB888)
            img=QtGui.QPixmap.fromImage(img)
            self.previousFrame = self.currentFrame
            return img
        except:
            return None

    def loadCalibration(self):
        """
        Load calibration from file and applies to the image
        Look at camera_cal.py final lines to see how that is done
        """
        pass
        #camera_matrix = [[219.88294045,0.,658.90478224],[0.,217.14503134,480.36304983],[0.,0.,1.]]
        #dist_coefs = [-1.06961183e-02,7.27806863e-05,-1.48339232e-04,-1.21672564e-03,9.97356821e-05]
        #h,w = self.currentFrame.shape[:2]

        #new_camera_matrix, roi = cv2.getOptimalNewCameraMatrix(camera_matrix,dist_coefs,(w,h),1,(w,h))
    	#while True:
            #ret, frame = cap.read()
            #rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2RGB)
            #undistorted = cv2.undistort(rgb_frame, camera_matrix, dist_coefs, None, new_camera_matrix)
            #cv2.imshow('camera', undistorted)
            #ch = 0xFF & cv2.waitKey(10)
            #if ch == 27:
                #break

